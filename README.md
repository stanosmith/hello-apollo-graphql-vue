# Hello Apollo + GraphQL + Vue

> Using vue-cli 3.0.0-beta.6

Check out the live demo: https://youtu.be/nq5q_gOOimA?t=766 


## Local Dev

```bash
# Start the Apollo/GraphQL server
npm run graphql-api

# Serve the Vue frontend
npm run serve
```
